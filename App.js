import React from 'react';
import { View } from 'react-native';
import BookList from '../BookStoreApp/Screens/BookList';

const App = () => {
  return (
    <View>
      <BookList />
    </View>
  );
};

export default App;
