import { createBottomTabNavigator } from "@react-navigation/bottomtabs";
import BookList from "../Screens/BookList";
const Tab = createBottomTabNavigator();
function SecondaryNavigator() {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarShowLabel: false,
            }}
            sceneContainerStyle={{ backgroundColor: "#a4c5e6" }}
        >
            <Tab.Screen
                name="BookList"
                component={BookList}
            />
        </Tab.Navigator>
    );
}
export default SecondaryNavigator;

