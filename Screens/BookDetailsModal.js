import React from 'react';
import { Modal, View, Text, Button } from 'react-native';

const BookDetailsModal = ({ book, visible, closeModal }) => {
  if (!book) {
    return null;
  }

  return (
    <Modal visible={visible} animationType="slide">
      <View>
        <Text>Title: {book.title}</Text>
        <Text>Author: {book.author}</Text>
        <Text>Description: {book.description}</Text>
        <Button title="Close" onPress={closeModal} />
      </View>
    </Modal>
  );
};

export default BookDetailsModal;
