import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import BookDetailsModal from './BookDetailsModal';

const BookList = () => {
  const [books, setBooks] = useState([
    { id: 1, title: 'Book 1', author: 'Author 1', description: 'Description 1' },
    { id: 2, title: 'Book 2', author: 'Author 2', description: 'Description 2' },
  ]);

  const [selectedBook, setSelectedBook] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const openBookDetails = (book) => {
    setSelectedBook(book);
    setModalVisible(true);
  };

  return (
    <View>
      <FlatList
        data={books}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => openBookDetails(item)}>
            <View>
              <Text>{item.title}</Text>
              <Text>{item.author}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
      <BookDetailsModal
        book={selectedBook}
        visible={modalVisible}
        closeModal={() => setModalVisible(false)}
      />
    </View>
  );
};

export default BookList;
